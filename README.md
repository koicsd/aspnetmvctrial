#My first ASP.NET MVC app
This is a trial project for me, where I tested myself how much time is it to get acquainted to *ASP.NET MVC* with *Code First*, *Identity* and *Bootstrap*.

##Description
The aim of the web-application is to create, list, modify and delete Formula-1 teams.
Any of these operations is only possible after authentication.
A Formula-1 team class should contain a *name*, a *year of foundation*, a *count of championships won* and a bool if *entry fee is payed*.
There should be a default user, named *admin* with password *f1test2018* in database.

##My conclusion
It took me some days to study the appropriate Visual-Studio project template and to get through initial stress.
[This set of *Pluralsight* videos](https://app.pluralsight.com/player?author=scott-allen&name=aspdotnet-mvc5-fundamentals-m1-introduction&mode=live&clip=0&course=aspdotnet-mvc5-fundamentals)
took me a day to watch but it helped me a lot. The next day I was able to make a working site.
After that, only some design and comfort improvement was necessary.

[If you want to checkout my *GitHub* profile, please click here.](https://www.github.com/KoicsD)

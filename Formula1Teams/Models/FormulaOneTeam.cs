﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Formula1Teams.Models
{
    [Table("FormulaOneTeams")]
    public class FormulaOneTeam
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        
        [Required]
        public string Name { get; set; }

        [Required]
        public int FoundationYear { get; set; }

        [Required]
        public int ChampionshipsWon { get; set; }

        [Required]
        public bool EntryFeePaid { get; set; }
    }
}
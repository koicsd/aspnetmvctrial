namespace Formula1Teams.Migrations
{
    using Formula1Teams.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Formula1Teams.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Formula1Teams.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            /*
            var hasher = new PasswordHasher();
            context.Users.AddOrUpdate(
                user => user.UserName,
                new ApplicationUser {
                    UserName = "admin",
                    Email = "admin@example.com",
                    PasswordHash = hasher.HashPassword("f1test2018")
                });
            */
            
            var userName = "admin";
            var email = "admin@example.com";
            var password = "f1test2018";
            if (!context.Users.Any(user => user.UserName == userName))
            {
                var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = new ApplicationUser { UserName = userName, Email = email, LockoutEnabled = true };
                manager.Create(user, password);
            }
            
        }
    }
}

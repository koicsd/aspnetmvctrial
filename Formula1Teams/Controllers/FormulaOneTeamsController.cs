﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Formula1Teams;
using Formula1Teams.Models;

namespace Formula1Teams.Controllers
{
    [Authorize]
    public class FormulaOneTeamsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: FormulaOneTeams
        public ActionResult List()
        {
            return View(db.Formula1Teams.ToList());
        }

        // GET: FormulaOneTeams/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormulaOneTeam formulaOneTeam = db.Formula1Teams.Find(id);
            if (formulaOneTeam == null)
            {
                return HttpNotFound();
            }
            return View(formulaOneTeam);
        }

        // GET: FormulaOneTeams/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FormulaOneTeams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,FoundationYear,ChampionshipsWon,EntryFeePaid")] FormulaOneTeam formulaOneTeam)
        {
            if (ModelState.IsValid)
            {
                db.Formula1Teams.Add(formulaOneTeam);
                db.SaveChanges();
                return RedirectToAction("List");
            }

            return View(formulaOneTeam);
        }

        // GET: FormulaOneTeams/Edit/5
        public ActionResult Edit(long? id, string returnUrl)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormulaOneTeam formulaOneTeam = db.Formula1Teams.Find(id);
            if (formulaOneTeam == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReturnUrl = EnsureUrlLocal(returnUrl);
            return View(formulaOneTeam);
        }

        // POST: FormulaOneTeams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,FoundationYear,ChampionshipsWon,EntryFeePaid")] FormulaOneTeam formulaOneTeam, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                db.Entry(formulaOneTeam).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect(EnsureUrlLocal(returnUrl));
            }
            return View(formulaOneTeam);
        }

        // GET: FormulaOneTeams/Delete/5
        public ActionResult Delete(long? id, string returnUrl)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormulaOneTeam formulaOneTeam = db.Formula1Teams.Find(id);
            if (formulaOneTeam == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReturnUrl = EnsureUrlLocal(returnUrl);
            return View(formulaOneTeam);
        }

        // POST: FormulaOneTeams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long? id)
        {
            FormulaOneTeam formulaOneTeam = db.Formula1Teams.Find(id);
            db.Formula1Teams.Remove(formulaOneTeam);
            db.SaveChanges();
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string EnsureUrlLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return returnUrl;
            }
            return Url.Action("List", "FormulaOneTeams");
        }
    }
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Formula1Teams.Startup))]
namespace Formula1Teams
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
